# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRCREV = "159729501c8c48d280978a86f3b580ae0dd9737d"

XEN_BRANCH = "upstream_maintenance_20230718"

SRC_URI = " \
        git://gitlab.com/Linaro/blueprints/automotive/xen-aosp/xen.git;branch=${XEN_BRANCH};protocol=https \
        file://0001-python-pygrub-pass-DISTUTILS-xen-4.15.patch \
        file://0001-Rename-to-xenphv.patch \
        file://0001-Enable-sdl-and-gl-test.patch \
        "
